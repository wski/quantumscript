'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var mechanics = {
  spin: undefined,
  getSpin: function getSpin() {
    if (mechanics.spin) {
      var spin = mechanics.spin;
      mechanics.spin = spin === 'up' ? 'down' : 'up';
      return spin;
    } else {
      var _spin = Math.random() > 0.5 ? 'up' : 'down';
      mechanics.spin = _spin === 'up' ? 'down' : 'up';
      return _spin;
    }
  }
};

/** Class representing quantum mechanics. */

var Quantum = function () {
  /**
  * Create a quantum object
  * @param {array} states - all possible states for our object
  * @param {*} particle - the current known state of the particle
  */

  function Quantum(states, particle) {
    _classCallCheck(this, Quantum);

    this.state = {
      particle: particle || 0,
      states: states,
      spin: undefined,
      constant: false,
      worlds: {}
    };

    this.superposition();
  }

  /**
  * Create all possible positions of our particle to emulate superposition / many worlds.
  * @param {array} states - all possible states for our object
  */


  _createClass(Quantum, [{
    key: 'superposition',
    value: function superposition() {
      var _this = this;

      this.state.states.map(function (state, i) {
        // Since a variable cannot be many things at once like in quantum theory,
        // we will use an object to simulte this object being in all possible states.
        _this.state.worlds[i] = _this.bindState(state);
      });
    }

    /**
    * If state is function it will compute and assign, else particle will be state.
    * @param {*} states - all possible states for our object
    * @returns {*} state - returns the quantum state of the object.
    */

  }, {
    key: 'bindState',
    value: function bindState(state) {
      try {
        return state(this.state.particle);
      } catch (e) {
        return state;
      }
    }

    /**
    * Many worlds decider, choose a state to occupy when being observed.
    * @returns {*} state - returns the quantum state of the object.
    */

  }, {
    key: 'manyWorlds',
    value: function manyWorlds() {
      // Right now the manyworlds descision is simply random, since It's unclear
      // to me how quantum particles decide exactly which state they will take.
      return this.state.worlds[Math.floor(Math.random() * Object.keys(this.state.worlds).length)];
    }

    /**
    * Interact with our particle
    * @param {function} modifier - modifier to run on particle
    */

  }, {
    key: 'interact',
    value: function interact(modifier) {
      this.state = _extends({}, this.state, {
        particle: modifier(this.state.particle),
        constant: false
      });
      this.superposition();
    }

    /**
    * Observe our quantum object
    * @param {array} states - all possible states for our object
    */

  }, {
    key: 'observe',
    value: function observe() {
      var _this2 = this;

      var particle = !this.state.constant ? function () {
        _this2.state = _extends({}, _this2.state, {
          constant: true
        });
        return _this2.manyWorlds();
      } : function () {
        return _this2.state.particle;
      };

      // We now know the particle to be something different, update it.
      this.state = _extends({}, this.state, {
        particle: particle(),
        spin: mechanics.getSpin()
      });

      return {
        value: this.state.particle,
        spin: this.state.spin
      };
    }
  }]);

  return Quantum;
}();

exports.default = Quantum;
