# QuantumScript

## What it is

This repo attempts to emulate some of the parts of the mechanics in quantum theory.

Because quantum physics cannot reasonably be translated into JavaScript, a lot of
how this library works is based on randomly choosing a set of superpositions when
observing the object.

You should also provide a set of known states to be used as the superposition of the object.

## Basic Usage

You can install QuantumScript with `npm i quantumscript`

Now let's setup a quantum dice, it will have 6 sides. This will mean that in 6
different worlds the die is facing every possible way. So the die at the time
of creation is in a superposition, and is as far as you know, rolled to every
possible solution. Schrödinger's die, if you will.
```javascript
const dice = new Quantum(
    [1, 2, 3, 4, 5, 6]
);
```
Now we'd like to observe the die and thus collapse it's superposition into one
possible state.

```javascript
dice.observe(); // 4
```

Great, so our die is 4 right? Let's check again.

```javascript
dice.observe(); // 4
```

Sweet, let's see if the number is even.

```javascript
dice.interact(die => !(die % 2)); // true
```

Awesome, 4 is even... it was 4 right? Let's be sure.

```javascript
dice.observe(); // 6
```

Because we've interacted with the die it had returned to a superposition, thus 
observing the die forced it to choose a new state.


## Functional Usage

You may pass functions in as the possible state, along with anything else, if the
quantum object collapses into a functional state, it will then run the previous value
on that function. You can set this up as shown below, passing the known state as you
fist observe as the second argument. This is necessary if you expect any results from
your functions.

```javascript
new Quantum(
    [Math.sqrt, Math.abs, Math.floor, Math.tan, 56, 'Oak Tree'],
    -6.8431234
);
```

## Some closing remarks

Many things this repo offers may be incorrect, and may need changing. I've been
building this as a way to try to understand the world at a nanoscopic scale. If you're
an expert at this sort of thing, please submit ideas via issues, or improvements via pull
requests! Enjoy this useless but interesting repo.

![rm](http://tvshowlovers.com/files/images/000/000/212.jpg)
