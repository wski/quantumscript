let mechanics = {
  spin: undefined,
  getSpin: function() {
    if (mechanics.spin) {
      let spin = mechanics.spin;
      mechanics.spin = (spin === 'up') ? 'down' : 'up';
      return spin;
    } else {
      let spin = (Math.random() > 0.5) ? 'up' : 'down';
      mechanics.spin = (spin === 'up') ? 'down' : 'up';
      return spin;
    }
  }
};

/** Class representing quantum mechanics. */
export default class Quantum {
  /**
  * Create a quantum object
  * @param {array} states - all possible states for our object
  * @param {*} particle - the current known state of the particle
  */
	constructor(states, particle) {
    this.state = {
      particle: particle || 0,
      states: states,
      spin: undefined,
      constant: false,
      worlds: {}
    };


  	this.superposition();
  }

  /**
  * Create all possible positions of our particle to emulate superposition / many worlds.
  * @param {array} states - all possible states for our object
  */
  superposition() {
  	this.state.states.map((state, i) => {
      // Since a variable cannot be many things at once like in quantum theory,
      // we will use an object to simulte this object being in all possible states.
    	this.state.worlds[i] = this.bindState(state);
    });
  }

  /**
  * If state is function it will compute and assign, else particle will be state.
  * @param {*} states - all possible states for our object
  * @returns {*} state - returns the quantum state of the object.
  */
  bindState(state) {
  	try {
    	return state(this.state.particle);
    } catch(e) {
    	return state;
    }
  }

  /**
  * Many worlds decider, choose a state to occupy when being observed.
  * @returns {*} state - returns the quantum state of the object.
  */
  manyWorlds() {
    // Right now the manyworlds descision is simply random, since It's unclear
    // to me how quantum particles decide exactly which state they will take.
    return this.state.worlds[
    	Math.floor(
      	Math.random() * Object.keys(this.state.worlds).length
      )
    ];
  }

  /**
  * Interact with our particle
  * @param {function} modifier - modifier to run on particle
  */
  interact(modifier) {
    this.state = {
      ...this.state,
      particle: modifier(this.state.particle),
      constant: false
    };
    this.superposition();
  }

  /**
  * Observe our quantum object
  * @param {array} states - all possible states for our object
  */
  observe() {
    const particle = (!this.state.constant) ? () => {
      this.state.constant = true;
      return this.manyWorlds();
    } : () => {
      return this.state.particle;
    };

    // We now know the particle to be something different, update it.
    this.state = {
      ...this.state,
      particle: particle(),
      spin: mechanics.getSpin()
    };

    return {
      value: this.state.particle,
      spin: this.state.spin,
    };
  }
}
